# Hello There! 👋 <br>
# Welcome to this repository!

## This repository is the ultimate one-stop-shop for all your academic resources needs.
## Find books📚, lecture slides🗈, programs💻, question papers📜, notes🗊, etc all treasured into this repository.

<div id="header" align="center">
  <img src="https://media.giphy.com/media/HYVZ1CGO7M3yMYVBxk/giphy.gif" width="30%"/>
</div>

## This repository aims to cache the flow of resources from seniors to juniors.
### Idea behind using GitHub as a platform is to give Git & GitHub exposure to tech students at the same time.
### Anybody can contribute their resources by putting up a pull request.

### Do give a star ⭐ for the repo, if it was helpful to you in anyway

#### Sharing is Caring❤️

# Our Star Contributors 😎

Zubin Shah | 20CSE1030 | https://www.linkedin.com/in/zubinshah1/

Ashish Singh | 21CSE1003 | https://www.linkedin.com/in/45h15h/

Harsh Patel | 19CSE1019 | https://www.linkedin.com/in/harshpatel63/

Niranjan Hebli | 20CSE1019 | https://www.linkedin.com/in/niranjan-hebli-333211211/






